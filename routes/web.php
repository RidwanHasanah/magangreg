<?php

Route::get('/', function () {
    return view('front-page.home2');
})->name('pendaftaran.pondokit');

Route::get('form-register','FrontPage\FrontPageController@formRegister')->name('form-register');
Route::get('form-search','FrontPage\FrontPageController@formSearch')->name('form-search');

Route::resource('psb','FrontPage\FrontPageController');
Route::post('psb/search','FrontPage\FrontPageController@search')->name('psb.search');

Auth::routes();


