@if (session('santri'))
<div id="alert-reg-success" class="container p-5 modal-regb ">
		<i class="btn btn-outline-danger btn-sm float-right btn-close">X</i>
	  <div>
		<h4>Pendaftaran Berhasil !!</h4>
		<p>Hai {{session('santri')}}, Selamat Kamu telah terdaftar sebagai calon Magang PONDOK IT. </p>
		<hr>
		<p class="mb-0">Silahkan buka Email Kamu, jika tidak ada di Inbox Kamu bisa melihatnya di folder email PROMOTION atau SOSIAL di Gmail Kamu atau Spam di yahoo dll.</p>
		<img class="my-3" style="width:90%; height:auto;" src="{{asset('images/email.png')}}" alt="Email"> <br>
		<a class="btn btn-primary" target="_blank"style="cursor:pointer" href="https://api.whatsapp.com/send?phone=6285228802828&amp;text=Assalamu%20Alaikum.%20Saya%20Sudah%20Daftar%20di%20Web%20Pendaftaran%20Magang%20PondokIT">JIKA SUDAH MENDAFTAR KONFIRMASI WHATSAPP  0852-2880-2828</a>
		  
	  </div>
	  <button class="btn btn-outline-danger float-right btn-close">close</button>
	</div>
@endif

@if (session('info'))
<div class="my-5 p-5">
	<div class="alert alert-info rupdate" role="alert">
		<h4 class="alert-heading">{{session('info')}}</h4>
	</div>
</div>
@endif

@if (session('success'))
<div class="my-5 p-5">
	<div class="alert alert-success rupdate" role="alert">
		<h4 class="alert-heading">{{session('success')}}</h4>
	</div>
</div>
@endif

@if (session('danger'))
<div class="my-5 p-5">
	<div class="alert alert-danger rupdate" role="alert">
		<h4 class="alert-heading">{{session('danger')}}</h4>
	</div>
</div>
@endif