<nav class="navbar navbar-expand-lg fixed-top">
    <a class="navbar-brand" href="/">
        <img src="{{asset('images/pondokit.png')}}">
    </a>
    <button id="btn-it" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="fa fa-navicon text-white"></span>
    </button>

    <div id="menu-it"class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a href="/" style="cursor:pointer" id="register-menu" class="nav-link">Pendaftaran</a>
        </li>
        <li class="nav-item">
            <a style="cursor:pointer" id="modal-contact-menu" class="nav-link">Kontak</a>
        </li> 
        </ul>
    </div>
</nav>