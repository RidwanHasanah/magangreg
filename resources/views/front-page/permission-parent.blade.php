<div class="container p-2 modal-reg " id="modal-permission2">
        <i class="btn btn-outline-danger btn-sm float-right btn-close" >X</i>
    <div class="row p-5">
        <div class="col-lg-12 col-md-12">
            <h3 class="text-center">
                    KETENTUAN MENGISI SURAT PERNYATAAN IZIN ORANG TUA
            </h3>
        <ul style="list-style: none;">
            <li>
                    <i class="material-icons md-18">done_outline</i> &nbsp; 
                    Unduh dokumen <b>SURAT PERNYATAAN IZIN ORANG TUA</b> melalui link berikut <a href="https://drive.google.com/file/d/1K5qzh497ZEbgagLoUdXa8NGqa12X2sn6/view?usp=sharing" target="_blank">ini</a>
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Cetak dokumen tersebut dengan ukuran kertas A4 dan berwarna.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Isilah data tersebut secara lengkap oleh orang tua anda.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Pastikan bahwa anda sudah benar-benar mendapatkan izin dan ridho dari orang tua anda.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Tanda tangan surat tersebut oleh orang tua anda.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Langkah selanjutnya scan/foto surat tersebut.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Format dokumen hasil scan/foto tersebut harus berbentuk (JPG).
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Upload file tersebut pada kolom formulir pendaftaran. <br>
                    <img style="width: 30%; height:auto;" src="{{asset('images/suratizin.png')}}" alt="surat izin orang tua">
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Info lebih lanjut mengenai hal ini silahkan hubungi email kami info.pondokit@gmail.com 
dengan subyek <b>SURAT PERNYATAAN IZIN ORANG TUA</b> .
            </li>
        </ul>
        </div>
        
    </div>
    <button class="btn btn-outline-danger float-right btn-close" >close</button>
</div>