<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta name="_token" content="{{csrf_token()}}" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('images/pondokit-icon.png')}}" type="image/gif" sizes="16x16">
    <link href="{{asset('themes/startbootstrap/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <title>Pendaftaran Magang Pondok IT</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" >
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/front-page.css')}}">
   {{-- Sweet Alert2 --}}
   <link rel="stylesheet" href="{{asset('themes/sweetalert2/sweetalert2.min.css')}}">
   <script src="{{asset('themes/sweetalert2/sweetalert2.min.js')}}"></script>  
   {{-- Ajax --}}
   {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" /> --}}
    <script src="{{asset('themes/startbootstrap/vendor/jquery/jquery.js')}}"></script>
  </head>
  <body>
    @include('front-page.menu')
    @include('front-page.contact')
    @yield('content')

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('js/jquery-3.3.1.slim.min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/front-page.js')}}"></script>
    {{-- Date Picker Online --}}
   {{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
   {{-- Date Picker Offline--}}
   <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
   <script src="{{asset('js/jquery-1.12.4.js')}}"></script>
   <script src="{{asset('js/jquery-ui.js')}}"></script>
  <script src="{{asset('js/validator/validator.min.js')}}"></script>
  {{-- <script src="{{asset('js/post-register.js')}}" ></script> --}}
  <script src="{{asset('js/search-register.js')}}" ></script>

    
  </body>
</html>