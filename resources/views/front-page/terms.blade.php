<div id="front-page">
    @include('layouts.patrials.alerts')
  <div class="mt-4">
    <div class="container-fluid header" id="terms">
      <div class="col-xl-12 mb-3 py-5">
        <div>
          <h1 class="text-center">Pendaftaran Magang Pondok IT</h1>
        </div>
      </div>
      <div class="row">
        <div id="logo" class="col-xl-6">
          <div class="col-xl-12">
            <img class="img-fluid my-3" src="{{asset('images/Magang.png')}}">
          </div>
          <div class="col-xl-12">
            <div class='console-container'><span id='text'></span>
              <div class='console-underscore' id='console'>&#95;</div>
            </div>
          </div>
        </div>
        <div id="syarat" class="col-xl-6">
          <ul class="syarat">
            <li class="">
              <h4><i class="material-icons">done_all</i> Persyaratan</h4>
            </li>
            <li class=""><i class="material-icons md-18">done_outline</i> Laki-Laki / Perempuan</li>
            <li class=""><i class="material-icons md-18">done_outline</i>Islam</li>
            {{-- <li class=""><i class="material-icons md-18">done_outline</i> Belum Menikah</li> --}}
            {{-- <li class=""><i class="material-icons md-18">done_outline</i> Lulusan SMP/SMK/SMA/MA/PONDOK/D3</li> --}}
            {{-- <li class=""><i class="material-icons md-18">done_outline</i> Menadapatkan Izin dan Ridho dari Orang Tua</li> --}}
            {{-- <li class=""><i class="material-icons md-18">done_outline</i> Tidak Sedang Bekerja/Kuliah</li> --}}
            {{-- <li class=""><i class="material-icons md-18">done_outline</i> Usia Minimal 16 Tahun</li> --}}
            {{-- <li class=""><i class="material-icons md-18">done_outline</i> Usia Maksimal 23 Tahun</li> --}}
            {{-- <li class=""><i class="material-icons md-18">done_outline</i> Memiliki Laptop* (Milik Sendiri atau Pinjam Laptop ke Kerabat Terdekat)</li> --}}
            <li class=""><i class="material-icons md-18">done_outline</i> Memiliki Minat/Passion di Bidang IT</li>
            <li class=""><i class="material-icons md-18">done_outline</i> Memiliki Semangat Menuntut Ilmu Agama Islam</li>
            {{-- <li class=""><i class="material-icons md-18">done_outline</i> Siap Untuk Belajar, Berkarya dan Berbagi di Pondok IT selama 3 tahun</li> --}}
            {{-- <li class=""><i class="material-icons md-18">done_outline</i> Siap Untuk Berjuang Mengembangkan Pondok IT</li> --}}
            <li class="mt-5"> Di Rekomendasikan Menggunakan Browser Chrome Desktop.</li>

          </ul>
        </div>
      </div>
  @include('front-page.talent')
  @include('front-page.permission-parent')
      {{-- <div class="row my-5 py-5 terms2 justify-content-center">
        <div class="col-xl-6 p-2">
          <div class="terms2bor p-3">
            <h4>PERSIAPKAN BERKAS-BERKAS BERIKUT INI SEBELUM MENDAFTAR UNTUK DIUNGGAH:</h4>
            <ul style="list-style: none">
              <li>
                <i class="material-icons md-18">done_outline</i> Hasil Talents Mapping. <br> Klik link berikut <i id="modal-talent"
                  class=" btn btn-primary">ini</i> untuk menuju halaman Materi Talents Mapping.
              </li>
              <li>
                <i class="material-icons md-18">done_outline</i> Surat pernyataan izin Orang Tua. <br> Klik link berikut
                <i class="btn btn-primary" id="modal-permission">ini</i> untuk mengetahui ketentuannya.
              </li>

              <li>
                <i class="material-icons md-18">done_outline</i> Hasil Tes IQ <br> Klik link berikut <a class="alink" href="http://memorado.eu"
                  target="_blank"> <i>ini</i> </a> untuk mengikuti tes IQ.
              </li>
              <li><i class="material-icons md-18">done_outline</i> Pas foto terbaru ukuran 4x6 (Maks. 500kb)</li>
              <li><i class="material-icons md-18">done_outline</i> Surat Rekomendasi dari Ustadz/Guru/Takmir Masjid/Sekolah*.
                <br> *tidak wajib / tapi sangat mendukung</li>

            </ul>
          </div>
        </div>
      </div> --}}

      <div class="container px-5 mt-5">
        <div class="row  justify-content-center">
          <div class="col-lg-6  col-md-6 pt-2 terms2">
            <h4 class="text-center py-2">Info</h4>
            <div class="row justify-content-center">
              <div class="col-lg-6 col-md-6 m-0 p-0">
                <div class="col-lg-12 col-md-12 m-0 w-100 py-2 text-white bg-danger" data-wow-delay="0.3s">
                  <b><i class="material-icons">contacts</i>&nbsp; Pondok Programmer</b>
                  <p>0896 4321 4321</p>
                </div>
                <div class="col-lg-12 col-md-12 m-0 w-100 py-2 text-danger wow  rotateInUpLeft slower" data-wow-delay="0.3s">
                  <b><i class="material-icons">contacts</i>&nbsp; Kontak WhatsApp Donasi</b>
                  <p>08778 7331 0910</p>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 m-0 p-0 ">
                <div class="col-lg-12 col-md-12 m-0 w-100 py-2 text-danger" data-wow-delay="0.3s">
                  <b><i class="material-icons">contacts</i>&nbsp; Pondok Multimedia</b>
                  <p>0813 2910 4101</p>
                </div>
                <div class="col-lg-12 col-md-12 m-0 bg-danger w-100 py-2 text-white wow  rotateInDownLeft slower" data-wow-delay="0.3s">
                  <b><i class="material-icons">mail_outline</i>&nbsp; Email</b>
                  <p> info.pondokit@gmail.com</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-12 pb-3">
        <div class="text-center my-4">
          <a href="{{route('form-register')}}" id="register-form-show"><button class="daftar btn btn-outline-danger btn-lg"">Daftar Sekarang</button></a>
        </div>
      </div>
      {{-- <div class="col-xl-12 p-2 terms2">
        <div class="text-center my-4" style="margin-top:auto;">
          <p>
            <b>*Pastikan di isi dengan benar, karena anda hanya dapat mendaftar sebanyak 1 kali</b>
          </p>
        </div>
      </div> --}}
    </div>
  </div>
</div>
