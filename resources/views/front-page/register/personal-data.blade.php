<h3><i class="material-icons">
    person
    </i> Data Diri</h3>
    <div class="row pt-2 mb-3" id="data-personal">
      <div class="col-xl-4 text-center">
        <h4 style="color: #2A5184;" class="bg-light py-1">Foto Anda</h4>
        <img style="max-width:78%;" class="shadow img-fluid img-thumbnail" src="{{asset('images/user.png')}}" id='img-upload'/>
        <div class="input-group upload">
          <label class="btn btn-outline-primary shadow" for="photo">Upload Foto</label>
          <input value="{{ old('photo') }}" required accept="image/jpeg,image/jpg,image/png," type="file" name="photo" id="photo">
          <span  class="errorval errorRegis" id="error_photo"></span>
        </div>
        @if ($errors->has('photo'))
        <span class="errorRegis">
          {{$errors->first('photo')}}
        </span>
        @endif
        <small class="text-success">
          Hanya file jpg/png/jpeg/gif, Ukuran Gambar maximal 500kb. <br>
        </small>
        </div>
      <div class="col-xl-8">
       <div class="row bg-light shadow pt-2 border mb-3">
          <div class="col-xl-4">
            <div class="form-group valid-form has-feedback">
              <label class="syarat" for="nama">Nama Lengkap</label>
              <input required  value="{{ old('name') }}" type="text" class="form-control" id="name" name="name" placeholder="Masukan nama" >
              <small class="text-success">Isi dengan nama lengkap anda.</small>
               <span  class="errorval errorRegis" id="error_name"></span>
               @if ($errors->has('name'))
                <span class="errorRegis">
                  {{$errors->first('name')}}
                </span>
                @endif
            </div>
          </div>
          <div class="col-xl-4">
            <div class="form-group valid-form has-feedback">
              <label class="syarat" for="divisi">Jenis Kelamin</label>
              <select class="form-control" name="sex" id="sex">
                <option value="">--Pilih--</option>
                <option id="sexp" value="Pria">Pria</option>
                <option id="sexw" value="Wanita">Wanita</option>                       
              </select>
              <span class="errorval errorRegis" id="error_sex"></span>
            </div>
          </div>
          <div class="col-xl-4">
            <div class="form-group has-feedback">
              <label class="syarat" for="tanggal_lahir">Tanggal Lahir</label>
              <input style="cursor: pointer; background:white;"  required value="{{ old('birth_date') }}" id="datepicker" type="text" readonly  class="form-control tanggal_lahir" name="birth_date" placeholder="hari/bulan/tahun" >
              <span  class="errorval errorRegis" id="error_birth_date"></span>
              @if ($errors->has('birth_date'))
              <span class="errorRegis">
                {{$errors->first('birth_date')}}
              </span>
              @endif
            </div>
          </div>
          <div class="col-xl-6">
            <div class="form-group has-feedback">
              <label class="syarat" for="province">Provinsi</label>
              <select class="form-control" name="province" id="province">
                <option value="Jakarta">Jakarta</option>
                <option value="Aceh">Aceh</option>
                <option value="Bali">Bali</option>
                <option value="Banten">Banten</option>
                <option value="Bengkulu">Bengkulu</option>
                <option value="Gorontalo">Gorontalo</option>
                <option value="Jambi">Jambi</option>
                <option value="Jawa Barat">Jawa Barat</option>
                <option value="Jawa Tengah">Jawa Tengah</option>
                <option value="Jawa Timur">Jawa Timur</option>
                <option value="Kalimantan Barat">Kalimantan Barat</option>
                <option value="Kalimantan Selatan">Kalimantan Selatan</option>
                <option value="Kalimantan Tengah">Kalimantan Tengah</option>
                <option value="Kalimantan Timur">Kalimantan Timur</option>
                <option value="Kalimantan Utara">Kalimantan Utara</option>
                <option value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung</option>
                <option value="Kepulauan Riau">Kepulauan Riau</option>
                <option value="Lampung">Lampung</option>
                <option value="Maluku">Maluku</option>
                <option value="Maluku Utara">Maluku Utara</option>
                <option value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                <option value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                <option value="Papua">Papua</option>
                <option value="Papua Barat">Papua Barat</option>
                <option value="Riau">Riau</option>
                <option value="Sulawesi Barat">Sulawesi Barat</option>
                <option value="Sulawesi Selatan">Sulawesi Selatan</option>
                <option value="Sulawesi Tengah">Sulawesi Tengah</option>
                <option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                <option value="Sulawesi Utara">Sulawesi Utara</option>
                <option value="Sumatera Barat">Sumatera Barat</option>
                <option value="Sumatera Selatan">Sumatera Selatan</option>
                <option value="Sumatera Utara">Sumatera Utara</option>
                <option value="Yogyakarta">Yogyakarta</option>
              </select>
              <span  class="errorval errorRegis" id="error_province"></span>
            </div>
          </div>
          <div class="col-xl-6" id="department2">
            <div class="form-group valid-form has-feedback">
              <label class="syarat" for="department">Jurusan Yang Dituju</label>
              <select class="form-control" name="department" id="department">
                <option id="dprogrammer" value="Programmer">Pondok Programmer</option>
                <option id="dmultimedia" value="Multimedia">Pondok Multimedia</option>
                <option id="dimers"  value="Imers">Pondok Imers</option>
                <option id="dcyber" value="Networkers">Pondok Networkers</option>                
              </select>
              <span class="errorval errorRegis" id="error_department"></span>
            </div>
          </div>
          <div class="col-xl-12">
            <div class="form-group has-feedback">
              <label class="syarat" for="addreess">Alamat Rumah</label>
              <textarea required type="text" id="address" class="form-control" name="address" placeholder=" Ex : Jl, Wijoyo Mulyo No.64 Rt/Rw 02/06 Banguntapan..." >{{ old('address') }}</textarea>
              <span  class="errorval errorRegis" id="error_address"></span>
              @if ($errors->has('address'))
              <span class="errorRegis">
                {{$errors->first('address')}}
              </span>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>