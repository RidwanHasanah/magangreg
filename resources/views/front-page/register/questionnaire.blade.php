<h3><i class="material-icons">
    list_alt
    </i> Kuesioner</h3>
<div class="row bg-light shadow pt-2 border mb-3">
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="ustad">3 Ustad Favorite </label>
      <input type="text" name="ust_idol" id="ust_idol" class="form-control">

      <span class="errorval errorRegis" id="error_ust_idol"></span>
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="rokok">Masih Merokok ? </label>
      <select name="smoke" id="smoke" class="form-control">
            <option value="Bukan Perokok">Bukan Perokok</option>
            <option value="Iya">Iya</option>
            <option value="Berusaha Berhenti">Berusaha Berhenti</option>
            <option value="Kadang - Kadang">Kadang - Kadang</option>
            <option value="Sudah Berhenti Total">Sudah Berhenti Total</option>
            <option value="Bukan Perokok Aktiv">Bukan Perokok Aktif</option>
          </select>
      <span class="errorval errorRegis" id="error_smoke"></span>
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="pemahaman">Pemahaman Agama</label>
      <input value="{{ old('religion') }}" type="text" id="religion" name="religion" required class="form-control" placeholder="Contoh: Muhammadiyah, NU dll…">
      <span class="errorval errorRegis" id="error_religion"></span> @if ($errors->has('religion'))
      <span class="errorRegis">
            {{$errors->first('religion')}}
          </span> @endif
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat">Mulai PKL / Magang</label>
      <input  style="cursor: pointer; background:white;"  id="datepicker-start" type="text" readonly type="text" name="start" class="form-control">
      @if ($errors->has('start'))
            <span class="errorRegis">
              {{$errors->first('start')}}
            </span> 
      @endif

      <span class="errorval errorRegis" id="error_start"></span>
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="ustad">Selesai PKL / Magang</label>
      <input  style="cursor: pointer; background:white;"  id="datepicker-finish" type="text" readonly type="text" name="finish" class="form-control">
      @if ($errors->has('finish'))
            <span class="errorRegis">
              {{$errors->first('finish')}}
            </span> 
      @endif

      <span class="errorval errorRegis" id="error_finish"></span>
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="Anda_tau"> Dari Mana Anda Mengetahui Pondok IT ? <br></label>
      <select class="form-control" name="pondok_know" id="pondok_know">
              {{-- <option value="">--Pilih--</option> --}}
              <option value="Orang Tua">Orang Tua</option>
              <option value="Guru / Ustadz">Guru / Ustadz</option>
              <option value="Saudara">Saudara</option>
              <option value="Teman">Teman</option>
              <option value="Facebook">Facebook</option>
              <option value="Youtube">Youtube</option>
              <option value="Instagram">Instagram</option>
              <option value="Yang Lain">Yang Lain</option>
          </select>
      <span class="errorval errorRegis" id="error_pondok_know"></span>
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="ustad">Siap Alokasi Dana</label>
      <p>
        Alokasi Dana akan di gunakan untuk
        <br>
        Makan-Tempat Tinggal-Listrik-Internet
      </p>
      <select class="form-control" name="pay" id="pay">
        <option value="500rb">500rb</option>
        <option value="600rb">600rb</option>
        <option value="700rb">700rb</option>
        <option value="800rb">800rb</option>
        <option value="900rb">900rb</option>
        <option value="1jt">1jt</option>

      </select>

      <span class="errorval errorRegis" id="error_ust_idol"></span>
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="skill">Jelaskan Tentang Skill IT Yang Sudah Dimiliki !</label>
      <textarea style="height: 100px;" name="it_skill" id="it_skill" class="form-control" row bg-lights="8" cols="70">{{ old('it_skill') }}</textarea>
      <span class="errorval errorRegis" id="error_it_skill"></span> 
      @if ($errors->has('it_skill'))
            <span class="errorRegis">
              {{$errors->first('it_skill')}}
            </span> 
      @endif
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="bahagia">Pelajaran yang Anda suka ?</label>
      <textarea class="form-control" style="height: 100px;" name="like_lesson" id="like_lesson" row bg-lights="8" cols="70">{{ old('like_lesson') }}</textarea>
      <span class="errorval errorRegis" id="error_like_lesson"></span> @if ($errors->has('like_lesson'))
      <span class="errorRegis">
                          {{$errors->first('like_lesson')}}
                        </span> @endif
    </div>
  </div>
</div>