<h3><i class="material-icons">
    group
    </i> Keluarga</h3>
    <div class="row bg-light shadow pt-2 border mb-4">
      <div class="col-xl-4">
        <div class="form-group has-feedback">
          <label class="syarat" for="phone_parent">Nomor Seluler Orang Tua</label>
          <input value="{{ old('phone_parent') }}" required  value="" type="text" class="form-control" id="phone_parent" name="phone_parent" >
           <span  class="errorval errorRegis" id="error_phone_parent"></span>
           @if ($errors->has('phone_parent'))
            <span class="errorRegis">
              {{$errors->first('phone_parent')}}
            </span>
            @endif
        </div>
      </div>
      <div class="col-xl-4">
        <div class="form-group has-feedback">
          <label class="syarat" for="father_name">Nama Ayah</label>
          <input value="{{ old('father_name') }}" required  value="" type="text" id="father_name" class="form-control" name="father_name" >
          <span  class="errorval errorRegis" id="error_father_name"></span>
          @if ($errors->has('father_name'))
          <span class="errorRegis">
            {{$errors->first('father_name')}}
          </span>
          @endif
        </div>
      </div>
      <div class="col-xl-4">
        <div class="form-group has-feedback">
          <label class="syarat" for="mother_name">Nama Ibu</label>
          <input value="{{ old('mother_name') }}" required  value="" type="text" class="form-control" id="mother_name" name="mother_name" >
          <span  class="errorval errorRegis" id="error_mother_name"></span>
          @if ($errors->has('mother_name'))
          <span class="errorRegis">
            {{$errors->first('mother_name')}}
          </span>
          @endif
        </div>
      </div>
    </div>