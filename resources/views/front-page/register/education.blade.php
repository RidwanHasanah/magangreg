<h3> <i class="material-icons">
    school
    </i> Pendidikan</h3>
<div class="row bg-light shadow pt-2 border mb-3">
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="jurusan">Jurusan  di Sekolah</label>
      <input value="{{ old('dep_school') }}" required value="" type="text" class="form-control" id="dep_school" name="dep_school">
      <span class="errorval errorRegis" id="error_dep_school"></span> @if ($errors->has('dep_school'))
      <span class="errorRegis">
            {{$errors->first('dep_school')}}
          </span> @endif
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="sekolah">Asal Sekolah</label>
      <input value="{{ old('school') }}" required value="" type="text" class="form-control" id="school" name="school">
      <span class="errorval errorRegis" id="error_school"></span> @if ($errors->has('school'))
      <span class="errorRegis">
            {{$errors->first('school')}}
          </span> @endif
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="sekolah">Nomor Wali Murid / Guru</label>
      <input value="{{ old('phone_teacher') }}" required value="" type="number" class="form-control" id="phone_teacher" name="phone_teacher">
      <span class="errorval errorRegis" id="error_phone_teacher"></span> 
      @if ($errors->has('phone_teacher'))
          <span class="errorRegis">
                {{$errors->first('phone_teacher')}}
          </span> 
      @endif
    </div>
  </div>
  <div class="col-xl-6">
    <div class="form-group has-feedback">
      <label class="syarat" for="organization">Pengalaman Organisai (Optional)</label>
      <textarea type="text" class="form-control" id="organization" name="organization">{{ old('organization') }}</textarea>
      <span class="errorval errorRegis" id="error_organization"></span> 
      @if ($errors->has('organization'))
      <span class="errorRegis">
              {{$errors->first('organization')}}
      </span> 
      @endif
    </div>
  </div>
  <div class="col-xl-6">
    <div class="form-group has-feedback">
      <label class="syarat" for="achievement">Prestasi (Optional)</label>
      <textarea type="text" class="form-control" id="achievement" name="achievement">{{ old('achievement') }}</textarea>
      <span class="errorval errorRegis" id="error_achievement"></span> 
      @if ($errors->has('achievement'))
      <span class="errorRegis">
                {{$errors->first('achievement')}}
      </span> 
      @endif
    </div>
  </div>
</div>