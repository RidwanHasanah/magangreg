@extends('front-page.home')
@section('content')
<div id="register-form">
    @include('layouts.patrials.alerts')
  <div class="form container pt-5 mt-5">
    <div class="alert alert-danger" style="display:none"></div>
    <h1 class="text-center text-danger">Form Pendaftaran</h1>
    <form id="formData" method="POST" action="{{route('psb.store')}}" enctype="multipart/form-data">
      {{-- method="POST" action="{{route('psb.store')}}" --}} 
      {{csrf_field()}} {{ method_field('POST') }}
      <div class="load col-xl-6 offset-xl-2" id="loadingDiv"><img src="{{asset('images/loading.gif')}}" alt="loading pendaftaran"></div>
          <div>@include('front-page.register.personal-data')</div>
          <div id="inc-contact">@include('front-page.register.contact')</div>
          <div id="inc-education">@include('front-page.register.education')</div>
          <div id="inc-family">@include('front-page.register.family')</div>
          <div id="inc-questionnaire">@include('front-page.register.questionnaire')</div>
          {{-- <div id="inc-document">@include('front-page.register.document')</div> --}}

          <div class="text-center row  py-4 mb-3">
            <div class="col-xl-12 terms2 p-3">
              <p> 
                <b> 
                  WAJIB !!! <br> <a class="btn btn-primary" target="_blank"style="cursor:pointer" href="https://api.whatsapp.com/send?phone=6285228802828&amp;text=Assalamu%20Alaikum.%20Saya%20Sudah%20Daftar%20di%20Web%20Pendaftaran%20Magang%20PondokIT">JIKA SUDAH MENDAFTAR KONFIRMASI WHATSAPP  0852-2880-2828</a><br>
                  {{-- Jika kirim data gagal silahkan dafta alternatif ke <a class="text-danger" href="https://goo.gl/forms/yS612uUUcRRMyza23" target="_blank">link berikut ini</a>  --}}
                </b>
              </p>
            </div>
            <div class="pt-3" id="inc-send">@include('front-page.register.send')</div>
          </div>
    </form>
  </div>
</div>
@endsection