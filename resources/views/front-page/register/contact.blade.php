<h3><i class="material-icons">
    perm_contact_calendar
    </i> Kontak</h3>
<div class="row bg-light shadow pt-2 border mb-3">
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="wa">Nomor WhatsApp</label>
      <input required  value="{{ old('wa') }}" type="text" class="form-control" id="wa" name="wa" placeholder="08xxxxxxxxxx" >
      <span  class="errorval errorRegis" id="error_wa"></span>
      @if ($errors->has('wa'))
      <span class="errorRegis">
        {{$errors->first('wa')}}
      </span>
      @endif
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="email">Email</label>
      <input required  value="{{ old('email') }}" type="email" id="email" class="form-control" name="email" placeholder="example@mail.com" >
      <span  class="errorval errorRegis" id="error_email"></span>
      @if ($errors->has('email'))
      <span class="errorRegis">
        {{$errors->first('email')}}
      </span>
      @endif
    </div>
  </div>
  <div class="col-xl-4">
    <div class="form-group has-feedback">
      <label class="syarat" for="facebook">Link Akun Facebook</label>
      <input required  value="{{ old('fb') }}" type="url" class="form-control" id="fb" name="fb" placeholder="https://www.facebook.com/contoh" ></input>
      <span style="color:green;" ><small>Isi dengan link profile facebook kamu</small></span>
      <span  class="errorval errorRegis" id="error_fb"></span>
      @if ($errors->has('fb'))
      <span class="errorRegis">
        {{$errors->first('fb')}}
      </span>
      @endif
    </div> 
  </div>
</div>