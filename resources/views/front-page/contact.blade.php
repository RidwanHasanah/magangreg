<div class="container p-5 modal-reg " id="modal-contact-menu2" style="">
    <i class="btn btn-outline-danger btn-sm float-right btn-close" >X</i>
    <h3 class="text-center">
        Kontak
    </h3>
    <div class="row p-5 justify-content-center">
        <div class="col-lg-6 col-md-6 m-0 p-0">
            <div class="col-lg-12 col-md-12 m-0 p-5 w-100 text-white bg-danger" data-wow-delay="0.3s">
                <h3><i class="material-icons">contacts</i>&nbsp; Pondok Programmer</h3>
                <h2>0877 8733 1091</h2>
            </div>
            <div class="col-lg-12 col-md-12 m-0 p-5 w-100 text-danger wow  rotateInUpLeft slower" data-wow-delay="0.3s">
                <h3><i class="material-icons">contacts</i>&nbsp; Kontak WhatsApp Donasi</h3>
                <h2>0852 2880 2880</h2>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 m-0 p-0 ">
            <div class="col-lg-12 col-md-12 m-0 p-5 w-100 text-danger" data-wow-delay="0.3s">
                <h3><i class="material-icons">contacts</i>&nbsp; Pondok Multimedia</h3>
                <h2>0813 2910 4101</h2>
            </div>
            <div class="col-lg-12 col-md-12 m-0 p-5 bg-danger w-100 text-white wow  rotateInDownLeft slower" data-wow-delay="0.3s">
                <h3><i class="material-icons">mail_outline</i>&nbsp; Email</h3>
                <h2> info.pondokit@gmail.com</h2>
            </div>
        </div>

    </div>
    <button class="btn btn-outline-danger float-right btn-close">close</button>
</div>
