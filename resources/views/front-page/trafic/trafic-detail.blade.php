<div class="container inc" id="s_profile">
    <h1>Profil Pendaftar</h1>
    <div id="s_selection"></div>
    <div class="row mt-3">
        <div class="col-xl-4 px-3 py-5" id="s_photo"></div>
        <div class="col-xl-8 shadow bg-light">
            <h3><i class="material-icons">person</i> Data Diri</h3>
            <table class="table">
                <tbody>
                <tr>
                    <th scope="row">Nama</th>
                    <td id="s_name"></td>
                </tr>
                <tr>
                    <th scope="row">Jenis Kelamin</th>
                    <td id="s_sex"></td>
                </tr>
                <tr>
                    <th scope="row">Jurusan</th>
                    <td id="s_department"></td>
                </tr>
                {{-- <tr>
                    <th scope="row">Tempat Lahir</th>
                    <td id="s_birth_place"></td>
                </tr>
                <tr>
                    <th scope="row">No Identitas</th>
                    <td id="s_ktp"></td>
                </tr> --}}
                <tr>
                    <th scope="row">Alamat</th>
                    <td id="s_address"></td>
                </tr>
                <tr>
                    <th scope="row">Provinsi</th>
                    <td id="s_province"></td>
                </tr>
                <tr>
                    <th scope="row">Hobi</th>
                    <td id="s_hobby"></td>
                </tr>
                <tr>
                    <th scope="row">Cita-Cita</th>
                    <td id="s_dreams"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>