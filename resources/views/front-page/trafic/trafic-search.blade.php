<div id="search-register" class="inc">
<div class="container my-5">
    <div class="row trafic-top">
        <div class="col col-lg-12 trafic">
            <div class="load" id="loadingSearch">
                <img src="{{asset('images/loading.gif')}}" alt="loading pendaftaran">
            </div>
            <form id="formSearch" action="" class="form-group" method="POST">
                    {{csrf_field()}}
                    {{ method_field('POST') }}
                <label for="input">Masukan Kode Unik Untuk Melihat status pendaftaranmu</label>
                <table>
                    <tr>
                        <td>
                            <input required class="form-control" type="text" name="id_santri" id="id_santri" placeholder="Contoh : 18JJ66758KK">
                        </td>
                        <td><button class="btn btn-info" >Cari</button></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

    @include('front-page.trafic.trafic-detail')
</div>
</div>