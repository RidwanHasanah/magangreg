<div class="container p-2 modal-reg " id="modal-talent2">
        <i class="btn btn-outline-danger btn-sm float-right btn-close" >X</i>
    <div class="row p-5">
        <div class="col-lg-12 col-md-12">
            <h3 class="text-center">
                    ANDA TELAH MEMASUKI HALAMAN TES TALENTS MAPPING. <br>
                    IKUTILAH LANGKAH-LANGKAH BERIKUT INI DENGAN SEKSAMA.
            </h3>
        <img src="{{asset('images/Petunjuk.png')}}" alt="">
        <h5 class="py-2">KETENTUAN MENGISI:</h5>
        <ul style="list-style: none;">
            <li>
                    <i class="material-icons md-18">done_outline</i> &nbsp; 
                    Isilah indentias diri dengan menggunakan huruf <b>KAPITAL</b>. 
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Untuk isian Pekerjaan diisi dengan: <b>CALON SANTRI</b>
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Untuk isian Organisasi diisi dengan: <b>PONDOK IT-TM JOGJA</b>
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Untuk isian Yang merekomendasikan anda untuk mengikuti tes ini diisi dengan: <b>Manager HRD</b>.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Bila sudah terisi semua lalu klik <b>NEXT</b>.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Setelah anda klik NEXT. Anda akan dihadapkan oleh beberapa pilihan.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    <b>Pilihlah minimal 7 </b> pernyataan yang <b>SANGAT SESUAI DENGAN DIRI ANDA</b>.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Bila sudah memilih 7 pernyataan lalu klik <b>NEXT</b>.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Pilihlah minimal 7 pernyataan yang <b>SANGAT TIDAK SESUAI DENGAN DIRI ANDA</b>
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Bila sudah memilih 7 pernyataan lalu klik <b>NEXT</b>.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    <b>Pilihlah minimal 7 </b> pernyataan yang <b> SESUAI DENGAN DIRI ANDA</b>.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Bila sudah memilih 7 pernyataan lalu klik <b>NEXT</b>.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Pilihlah minimal 7 pernyataan yang <b> TIDAK SESUAI DENGAN DIRI ANDA</b>
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Bila sudah memilih 5 pernyataan lalu klik <b>FINISH</b>.
            </li>
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Selanjutnya <b>klik save</b> pada hasil Talents Mapping anda dalam format PDF<br>.
                    Nama file PDF nantinya secara otomatis adalah: <b>(st30_NAMA ANDA)</b>.
            </li>
            {{-- <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    Kemudia ganti nama file dengan menambahkan nomor ID pendaftaran anda menjadi <b>(st30_NAMA ANDA)</b>.
            </li> --}}
            <li>
                    <i class="material-icons md-18">done_outline</i>&nbsp;
                    <b>Upload hasil Talents Mapping</b> tersebut pada kolom <b>formulir pendaftaran</b> kami.
            </li>
        </ul>
        <a class="btn btn-outline-primary" href="https://temabakat.com/id/index.php/main/disp/tes" target="_blank">Mulai Tes</a>
            

        </div>
        
    </div>
    <button class="btn btn-outline-danger float-right btn-close" >close</button>
</div>