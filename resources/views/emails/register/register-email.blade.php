@component('mail::message')
Assalamu alaikum, {{$reg->name}}

Selamat Kamu telah terdaftar sebagai calon Magang Pondok IT pada Jurusan Pondok {{$reg->department}}.

{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Salam Hormat,<br>
{{ config('app.name') }}
@endcomponent
