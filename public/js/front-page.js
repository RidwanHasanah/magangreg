$(document).ready( function() {
// Menu
$('#btn-it').click(function(){
	$('#menu-it').toggle();
})

// Modal Start
function modalRegOpen(id,id2){
	id.click(function () {
		id2.slideUp('slow').show()
		$("html, body").animate({ scrollTop: 0 }, "slow");
		console.log('klik')
	  })
}
function modalRegClose(id,id2){
	id.click(function () {
		$('.modal-regb').slideDown('slow').hide()
		id2.slideDown('slow').hide()
		console.log('klik')
	  })
}
modalRegOpen($('#modal-talent'),$('#modal-talent2'))
modalRegOpen($('#modal-permission'),$('#modal-permission2'))
modalRegOpen($('#modal-contact-menu'),$('#modal-contact-menu2'))

modalRegClose($('.btn-close'),$('.modal-reg'))


// Modal Talent End

	function valDoc(id){
		id.bind('change', function() {
			var siz = this.files[0].size;
			if(siz >= 500000){
				swal({
					title: 'Oops',
					text: 'Maaf Ukuran File Melebihi Kapasitas, Ukuran maximal file adalah 500kb',
					type: 'error',
				  })
			}
		  });
	}

	valDoc($('#photo'));
	
	valDoc($('#permission_parent'));
	valDoc($('#salary'));

	function valDocTalent(id){
		id.bind('change', function() {
			var siz = this.files[0].size;
			if(siz >= 1300000){
				swal({
					title: 'Oops',
					text: 'Maaf Ukuran File Melebihi Kapasitas, Ukuran maximal file adalah 1.2mb',
					type: 'error',
				  })
			}
		  });
	}
	valDocTalent($('#talent_mapping'));

	$('#submit').hover(function(){

		var divisi = $('#divisi').val();
		var errorDivisi = $('#errorDivisi');

		function error(id,value){

			id.html(value).show(); //menampilkan text string yang ada
			id.show();//menampilkan span
		}
		console.log('bismillah')

		if ($('#photo').val().length == 0) {
			swal({
                title: 'Oops',
                text: 'kamu harus upload photo terlebih dahulu',
                type: 'error',
              }).then(error($('#errorDivisi'),'kamu harus upload photo terlebih dahulu'))
		}

	})
	

	/**
	 * SPA
	 */
	// Home
	 $('#register-menu').click(function(){
		$('#front-page').slideDown('slow').show();
		$('#register-form').slideDown("slow").hide();
		$('#search-register').slideUp('slow').hide();
		$('#alert-reg-success').slideUp('slow').hide();
		$('#modal-contact-menu2').slideDown("slow").hide();
	})
	//Check 
	$('#register-check').click(function(){
		$('#search-register').slideDown('slow').show();
		$('#front-page').slideUp('slow').hide();
		$('#register-form').slideDown("slow").hide();
		$('#modal-contact-menu2').slideDown("slow").hide();

	})
	// Form Register
	$('#register-form-show').click(function(){
		$('#register-form').slideDown("slow").show()
		$('#front-page').slideUp('slow').hide();
		$('#alert-reg-success').slideUp('slow').hide();
		$('#search-register').slideUp('slow').hide();
		$('#modal-contact-menu2').slideDown("slow").hide();

	})


	/** 
	 * ======================================
	 * ==== Show Form Register Start =========
	 * ======================================
	 * */ 


	function formShow(id,target){
		$(id).blur(function(){
			$(target).slideDown('slow').show();
		})
		if(id == '#know'){
			console.log('=====Know')
		}
		
	}
	// formShow('#dreams','#inc-contact')
	// formShow('#instagram','#inc-education')
	// formShow('#school','#inc-family')
	// formShow('#know','#inc-questionnaire')
	// formShow('#spirit','#inc-document')
	// formShow('#permission_parent','#inc-send')

	function formShow3(id,target){
		$(id).hover(function(){
			$(target).slideDown('slow').show();
		})
	}
	// formShow3('#dreams','#inc-contact')
	// formShow3('#fb','#inc-education')
	// formShow3('#school','#inc-family')
	// formShow3('#know','#inc-questionnaire')
	// formShow3('#spirit','#inc-document')
	// formShow3('#permission_parent','#inc-send')

	/** 
	 * ======================================
	 * ==== Show Form Register End =========
	 * ======================================
	 * */ 

	/*===== Date Picker Start =====*/
	$("#datepicker").datepicker({
		dateFormat:"dd-mm-yy",
		changeMonth: true,
		changeYear: true,
		yearRange: "1990:2004"});

	// Start PKL
	$("#datepicker-start").datepicker({
		dateFormat:"dd-mm-yy",
		changeMonth: true,
		changeYear: true,
		yearRange: "1990:2030"});

	// Finish PKL
	$("#datepicker-finish").datepicker({
		dateFormat:"dd-mm-yy",
		changeMonth: true,
		changeYear: true,
		yearRange: "1990:2030"});
	
	
	/*===== Date Picker End =====*/

	/**=== sex and Divisi Start === */
	// $('#department').click(function(){
	// 	console.log($('#sex').val())
	// 	if($('#sex').val() == ''){
	// 		$('#dmuslimah').slideUp('slow').hide();
	// 		$('#dprogrammer').slideUp('slow').hide();
	// 		$('#dmultimedia').slideUp('slow').hide();
	// 		$('#dimers').slideUp('slow').hide();
	// 		$('#dcyber').slideUp('slow').hide();
	// 		$('#dmanajer').slideUp('slow').hide();
	// 		$('#dagro').slideUp('slow').hide();
	// 		$('#dkoki').slideUp('slow').hide();
	// 		console.log('kosong')
	// 	}else if($('#sex').val() == 'Pria'){
	// 		$('#dprogrammer').slideUp('slow').show();
	// 		$('#dmultimedia').slideUp('slow').show();
	// 		$('#dimers').slideUp('slow').show();
	// 		$('#dcyber').slideUp('slow').show();
	// 		$('#dmanajer').slideUp('slow').show();
	// 		$('#dagro').slideUp('slow').show();
	// 		$('#dkoki').slideUp('slow').show();
	// 		$('#dmuslimah').slideUp('slow').hide();
	// 		console.log('Pria')
	// 	}else if($('#sex').val() == 'Wanita'){
	// 		$('#dmuslimah').slideUp('slow').show();
	// 		$('#dprogrammer').slideUp('slow').hide();
	// 		$('#dmultimedia').slideUp('slow').hide();
	// 		$('#dimers').slideUp('slow').hide();
	// 		$('#dcyber').slideUp('slow').hide();
	// 		$('#dmanajer').slideUp('slow').hide();
	// 		$('#dagro').slideUp('slow').hide();
	// 		$('#dkoki').slideUp('slow').hide();
	// 		console.log('Wanita')
	// 	}

	// })
	/**=== sex and Divis End === */

	

    $(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [label]);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#photo").change(function(){
        readURL(this);
    });     
});



// function([string1, string2],target id,[color1,color2])    
consoleText(['Programmer', 'Multimedia', 'Imers','Manajer','Muslimah', 'Networkers'], 'text',['#212529','#212529','#212529','#212529']);

function consoleText(words, id, colors) {
if (colors === undefined) colors = ['#fff'];
var visible = true;
var con = document.getElementById('console');
var letterCount = 1;
var x = 1;
var waiting = false;
var target = document.getElementById(id)
target.setAttribute('style', 'color:' + colors[0])
window.setInterval(function() {

if (letterCount === 0 && waiting === false) {
  waiting = true;
  target.innerHTML = words[0].substring(0, letterCount)
  window.setTimeout(function() {
    var usedColor = colors.shift();
    colors.push(usedColor);
    var usedWord = words.shift();
    words.push(usedWord);
    x = 1;
    target.setAttribute('style', 'color:' + colors[0])
    letterCount += x;
    waiting = false;
  }, 1000)
} else if (letterCount === words[0].length + 1 && waiting === false) {
  waiting = true;
  window.setTimeout(function() {
    x = -1;
    letterCount += x;
    waiting = false;
  }, 1000)
} else if (waiting === false) {
  target.innerHTML = words[0].substring(0, letterCount)
  letterCount += x;
}
}, 120)
window.setInterval(function() {
if (visible === true) {
  con.className = 'console-underscore hidden'
  visible = false;

} else {
  con.className = 'console-underscore'

  visible = true;
}
}, 400)
	


	function checkVal(//function for check input
		name, //long string check and id 
		value, //value string for show
		error,//<span> id in the html for the show();
		condition //Kondisi if
		){
			name.focusout(function(){
				if (name.val().length < condition) {
					error.html(value).show(); //menampilkan text string yang ada
					error.show();//menampilkan span
				}else{
					error.hide(); //jika false maka hide
				}
			})
		
	}
	/**
	 * Validasi Data Diri
	 */
	checkVal($('#photo'),' harus di isi',$('#error_photo'),1)
	checkVal($('#name'),'Nama harus lebih dari 4 karakter',$('#error_name'),4)
	checkVal($('#sex'),'Kamu harus pilih jenis kelamin dahulu',$('#error_sex'),1)
	checkVal($('#ktp'),'Ktp / SIM harus di isi lebih dari 13 karakter',$('#error_ktp'),13)
	checkVal($('#city'),'Kota harus di isi',$('#error_city'),1)
	checkVal($('#birth_place'),'Tempat Lahir harus di isi minimal 3 karakter ',$('#error_birth_place'),3)
	// checkVal($('#datepicker'),'Tanggal Lahir harus di isi sesuai format 11/11/2011 10 karakter ',$('#error_birth_date'),8)
	checkVal($('#address'),'Alamat harus di isi lebih dari 10 karakter',$('#error_address'),9)
	checkVal($('#province'),'Kota harus di isi lebih dari 3 karakter',$('#error_province'),3)
	checkVal($('#department'),'Jurusan harus di isi',$('#error_department'),1)
	checkVal($('#hobby'),'Hobi harus di isi lebih dari 4 karakter',$('#error_hobby'),4)
	checkVal($('#dreams'),'Impian harus di isi lebih dari 5 karakter',$('#error_dreams'),5)

	/**
	 * Kontak
	 */
	checkVal($('#phone'),'Nomor Hp harus di isi lebih dari 9 karakter',$('#error_phone'),9)
	checkVal($('#wa'),'Nomor WA harus di isi lebih dari 9 karakter',$('#error_wa'),9)
	checkVal($('#email'),'Email harus di isi lebih dari 5 karakter',$('#error_email'),5)
	checkVal($('#fb'),'Facebook harus di isi Link',$('#error_fb'),1)
	$('#instagram').focusout(function(){
		var insval = $('#instagram').val(); 
		var ins = $('#instagram').val().split("").shift()
		if(insval != 0){
			if(ins != '@'){
				$('#error_instagram').html('Anda harus menambahkan tanda @ pada awal huruf').show(); //menampilkan text string yang ada
				$('#error_instagram').show()
			}else{
				$('#error_instagram').hide()
			}
		}else{
			$('#error_instagram').hide()
		}
		
	})
	/**
	 * Education
	 */
	checkVal($('#education'),'Pendidikan Terakhir harus di isi',$('#error_education'),1)
	checkVal($('#dep_school'),'Jurusan Sekolah harus di isi lebih dari 2 karakter',$('#error_dep_school'),2)
	checkVal($('#school'),'Asal Sekolah harus di isi lebih dari 4 karakter',$('#error_school'),4)

	/**
	 * Family
	 */
	checkVal($('#amount_parent'),'Jumlah orang tua harus di isi',$('#error_amount_parent'),1)
	checkVal($('#phone_parent'),' harus di isi lebih dari 9 karakter',$('#error_phone_parent'),9)
	checkVal($('#father_name'),'Nama Ayah harus di isi lebih dari 3 karakter',$('#error_father_name'),3)
	checkVal($('#mother_name'),'Nama Ibu harus di isi lebih dari 3 karakter',$('#error_mother_name'),3)
	checkVal($('#father_job'),'Pekerjaan Ayah harus di isi lebih dari 3 karakter',$('#error_father_job'),3)
	checkVal($('#mother_job'),'Pekerjaan Ibu harus di isi lebih dari 3 karakter',$('#error_mother_job'),3)
	checkVal($('#income_parent'),' harus di isi ',$('#error_income_parent'),1)
	checkVal($('#amount_brother'),' harus di isi ',$('#error_amount_brother'),1)
	// checkVal($('#infaq_parent'),' harus di isi lebih dari 3  karakter',$('#error_infaq_parent'),3)
	// checkVal($('#know'),' harus di isi lebih dari 3 karakter',$('#error_know'),3)

	/**
	 * Questionnaire
	 */
	checkVal($('#laptop'),' harus di isi',$('#error_laptop'),1)
	checkVal($('#iq'),' harus di isi',$('#error_iq'),1)
	checkVal($('#quran'),' harus di isi',$('#error_quran'),1)
	checkVal($('#it_skill'),'Skill IT harus di isi lebih dari 3 karakter',$('#error_it_skill'),3)
	checkVal($('#talent'),' harus di isi lebih dari 10 karakter',$('#error_talent'),9)
	checkVal($('#bad'),' harus di isi lebih dari  10 karakter',$('#error_bad'),9)
	checkVal($('#people_bad'),' harus di isi lebih dari  10 karakter',$('#error_people_bad'),9)
	checkVal($('#idol'),' harus di isi lebih dari 3 karakter',$('#error_idol'),3)
	checkVal($('#book'),' harus di isi lebih dari 3 karakter',$('#error_book'),3)
	checkVal($('#ust_idol'),' harus di isi',$('#error_ust_idol'),1)
	checkVal($('#burden'),' harus di pilih',$('#error_burden'),1)
	checkVal($('#smoke'),' harus di isi',$('error_#smoke'),1)
	checkVal($('#couple'),' harus di isi',$('#error_couple'),1)
	checkVal($('#burden_private'),' harus di isi',$('#error_burden_private'),1)
	checkVal($('#creation'),' harus di isi',$('#error_creation'),1)
	checkVal($('#experience'),' harus di isi',$('#error_experience'),1)
	checkVal($('#religion'),' harus di isi lebih dari  karakter',$('#error_religion'),1)
	checkVal($('#charity'),' harus di isi lebih dari 10 karakter',$('#error_charity'),1)
	checkVal($('#five_m'),' harus di isi lebih dari 20 karakter',$('#error_five_m'),20)
	checkVal($('#bad'),' harus di isi lebih dari 10 karakter',$('#error_bad'),9)
	checkVal($('#bad_people'),' harus di isi lebih dari 10 karakter',$('#error_bad_people'),9)
	checkVal($('#angry'),' harus di isi minimal 10karakter ',$('#error_angry'),9)
	checkVal($('#happy'),' harus di isi minimal 10karakter',$('#error_happy'),9)
	checkVal($('#alms'),' harus di isi',$('#error_alms'),1)
	checkVal($('#volunteer'),' harus di isi',$('#error_volunteer'),1)
	checkVal($('#fight'),' harus di isi',$('#error_fight'),1)
	checkVal($('#pondok_dreams'),' harus di isi',$('#error_pondok_dreams'),1)
	checkVal($('#alms'),' harus di isi',$('#error_alms'),1)
	checkVal($('#alms2'),' harus di isi',$('#error_alms2'),1)
	checkVal($('#alms3'),' harus di isi',$('#error_alms3'),1)
	checkVal($('#rules'),' harus di isi',$('#error_rules'),1)
	checkVal($('#pondok_minus'),' harus di isi',$('#error_pondok_minus'),1)
	checkVal($('#pondok_know'),' harus di isi',$('#error_pondok_know'),1)
	checkVal($('#ten_dreams'),' harus di isi lebih dari 10 karakter',$('#error_ten_dreams'),9)
	checkVal($('#whishes'),' harus di isi lebih dari 10 karakter',$('#error_whishes'),9)
	checkVal($('#reason'),' harus di isi lebih dari 10 karakter',$('#error_reason'), 10)
	checkVal($('#spirit'),' harus di isi lebih dari 10 karakter',$('#error_spirit'),9)
	checkVal($('#personal_data'),' harus di isi',$('#error_personal_data'),1)
	checkVal($('#like_lesson'),' harus di isi',$('#error_like_lesson'),1)
	checkVal($('#unlike_lesson'),' harus di isi',$('#error_unlike_lesson'),1)
	checkVal($('#eng_ability'),' harus di isi',$('#error_eng_ability'),1)
	
	
}