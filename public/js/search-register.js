$(document).ready(function(){
    $('#formSearch').on('submit',function(e){
        
        // Loading
        var $loading = $('#loadingSearch').hide();
            $(document)
        .ajaxStart(function () {
            $loading.show();
        })
        .ajaxStop(function () {
            $loading.hide();
        });

        // Header
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'psb/search',
            data : new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                if (data == 'error') {
                    swal({
                        title: 'Maaf Tidak Ada',
                        text: 'Tidak ada data dari kode Unik yang kamu masukan, Silahkan periksa kembali kode Unik kamu',
                        type: 'info',
                      })
                }else{
                    if (data.selection >= 6) {
                        $('#s_selection').append('<button class="btn btn-warning push-right px-5" ><b>Telah Lulus Seleksi</b></button>')
                    }else if(data.failed == true){
                        $('#s_selection').append('<button class="btn btn-warning push-right px-5" ><b>Tidak Lulus Seleksi</b></button>')
                    }else{
                        $('#s_selection').append('<button class="btn btn-warning push-right px-5" ><b>Dalam Proses Seleksi</b></button>')
                    }
                    $('#s_photo').append('<img class="img-fluid shadow bg-light" src="'+window.location.origin+'/storage/photos/'+data.photo+'">')
                    $('#s_profile').slideDown('slow').show()
                    $('#s_name').text(data.name)
                    $('#s_sex').text(data.sex)
                    $('#s_department').text(data.department)
                    // $('#s_birth_place').text(data.birth_place)
                    // $('#s_ktp').text(data.ktp)
                    $('#s_address').text(data.address)
                    $('#s_province').text(data.province)
                    $('#s_hobby').text(data.hobby)
                    $('#s_dreams').text(data.dreams)
                }
            },
            error: function(data){
                console.log('Error => ', data)
            }
        })


    })
})