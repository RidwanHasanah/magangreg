<?php

namespace App\Events\Register;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use App\Models\Internship;

class RegisterEvent
{
    use Dispatchable, SerializesModels;
    public $reg;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Internship $reg)
    {
        $this->reg = $reg;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
