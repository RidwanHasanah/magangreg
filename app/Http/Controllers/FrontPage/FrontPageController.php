<?php

namespace App\Http\Controllers\FrontPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Internship;
use Illuminate\Support\Facades\Auth; //untukmenggunakan Controller Auth
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Events\Register\RegisterEvent;
use Illuminate\Support\Facades\Mail;

class FrontPageController extends Controller
{
    public function index()
    {
        //
    }
    public function formRegister(){
        return view('front-page.register.register');
    }

    public function formSearch(){
        return view('front-page.trafic.trafic-search');
    }

    public function store(Request $request)
    {
        $reg = new Internship;
        $target = [
            // Personal Data
            'photo' => 'required|image ',
            'name' => 'required|min:5|max:150',
            'birth_date' => 'required|max:150',       
            'address' => 'required',
            'province' => 'required',
            'department' => 'required',
            'sex' => 'required',
            // Contact
            'wa' => 'required|unique:internships|numeric',
            'fb' => 'required|max:150',
            'email' => 'required|unique:internships|email|max:150',
            // Education
            'dep_school' => 'required|max:150',
            'school' => 'required|max:150',
            'phone_teacher' => 'required|numeric',
            // Family
            'phone_parent' => 'required|numeric',
            'father_name' => 'required|max:150',
            'mother_name' => 'required|max:150',
            // Kuesioner
            'ust_idol' => 'required|max:150',
            'smoke' => 'required',
            'religion' => 'required|max:150',
            'pondok_know' => 'required',
            'it_skill' => 'required',
            'like_lesson' => 'required',
            'pay' => 'required',
            'start' => 'required',
            'finish' => 'required'
            
        ];
        $msg = [
            // Personal Data
            'photo.required' => 'Foto diri  Wajib di isi',
            'photo.mimes' => 'Format Foto harus JPG / JPEG / GIF / PNG',
            'photo.image' => 'Foto harus bertipe format JPEG / PNG / JPG / GIF',
            'name.required' => 'Wajib Mengisi Nama',
            'name.max' => 'Nama Maximal 150 karakter',
            'birth_date.required' => 'Tanggal Lahir Wajib di Isi',
            'address.required' => 'Alamat wajib di isi',
            'province.required' => 'Provinsi wajib di isi',
            'sex.required' => 'Jenis Kelamin Wajib di isi',
            'department.required' => 'Jurusan Wajib di isi',

            // Contact
            'wa.required' => 'Nomor WhatsApp Wajib di isi',
            'wa.unique' => 'Nomor WhatsApp sudah terdaftar',
            'wa.numeric' => 'Nomor WhatsApp harus bertipe angka',
            'email.required' => 'Email Wajib di isi',
            'email.unique' => 'Email sudah terdaftar gunakan email lain',
            'email.email' => 'Harus di isi Alamat Email',
            'email.max' => 'Email Maximal 150 karakter',
            'fb.required' => 'Link Facebook Wajib di isi',

            // Education
            'dep_school.required' => 'Jurusan Sekolah Wajib di isi',
            'dep_school.max' => 'Jurusan Sekolah Maximal 150 karakter',
            'school.required' => 'Sekolah Asal Wajib di isi',
            'school.max' => 'Sekolah Maximal 150 karakter',
            'phone_teacher.required' => 'Nomor HandPhone Wali Murid / Guru Wajib di isi',

            // Family
            'phone_parent.required' => 'Nomor Telepon Orang Tua Wajib di isi',
            'phone_parent.numeric' => 'Nomor Telepon Orang Tua harus bertipe angka',
            'father_name.required' => 'Nama Ayah Wajib di isi',
            'father_name.max' => 'Nama Ayah Maximal 150 karakter',
            'mother_name.required' => 'Nama Ibu Wajib di isi',
            'mother_name.max' => 'Nama Ibu Maximal 150 karakter',

            // Kuesioner
            'ust_idol.required' => 'Ustadz Idola Wajib di isi',
            'ust_idol.max' => 'Ustadz Idola Maximal 150 karakter',
            'smoke.required' => 'Perokok atau Bukan  Wajib di isi',
            'religion.required' => 'Pemahaman Agama Wajib di isi',
            'religion.max' => 'Pemahaman Agama Maximal 150 karakter',
            'pondok_know' => 'Wajib di isi',
            'it_skill.required' => 'Skill IT yang dimiliki Wajib di isi',
            'like_lesson.required' => 'Pelajaran yang di suka Wajib di isi',
            'pay.required' => 'Wajib di isi',
            'start.required' => 'Wajib di isi',
            'finish.required' => 'Wajib di isi'
        ];

        $request->validate($target, $msg);
            // Personal Data
            $reg->name = $request->name;
            $reg->sex = $request->sex;
            $reg->birth_date = date('Y-m-d', strtotime($request->birth_date));
            $reg->province = $request->province;
            $reg->department = $request->department;
            $reg->address = $request->address;
            // Contact
            $reg->wa = $request->wa;
            $reg->email = $request->email;
            $reg->fb = $request->fb;
            // Education
            $reg->dep_school = $request->dep_school;
            $reg->school = $request->school;
            $reg->organization = $request->organization;
            $reg->achievement = $request->achievement;
            $reg->phone_teacher = $request->phone_teacher;
            // Family
            $reg->phone_parent = $request->phone_parent;
            $reg->father_name = $request->father_name;
            $reg->mother_name = $request->mother_name;
            // Kuesioner
            $reg->ust_idol = $request->ust_idol;
            $reg->smoke = $request->smoke;
            $reg->religion = $request->religion;
            $reg->pondok_know = $request->pondok_know;
            $reg->it_skill = $request->it_skill;
            $reg->like_lesson = $request->like_lesson;
            $reg->pay = $request->pay;
            $reg->start = date('Y-m-d', strtotime($request->start));
            $reg->finish = date('Y-m-d', strtotime($request->finish));

            $reg->cancel = false;
            $reg->status = 1;
            
            
        // Photo
            if ($request->hasFile('photo')) {
                    $photo = $request->file('photo');
                    $photoName = rand(1,99) . $photo->getClientOriginalName();
                    $photo->storeAs('photos', $photoName);
                    $reg->photo = $photoName;
            }

            $reg->save();

            if ($reg->save()) {
            
            // Send Mail
                event(new RegisterEvent($reg));
                return redirect()->route('pendaftaran.pondokit')->with('santri',$request->name);
            } else {
                $reg->email = 'ridwanhasanah3@gmail.com';
                $reg->id_santri = $id_santri.' Gagal Gagal Daftar';
                event(new RegisterEvent($reg));
                return url('/')->with('santri',$id_santri);
            }

    }

    
    /**
     * Search Trafic
     */
    public function search(Request $request)
    {
        $reg = Register::where('id_santri', $request->id_santri)->count();
        $reg2 = Register::where('id_santri', $request->id_santri)->first();
        if ($reg == 1) {

            return response()->json($reg2);
        } else {
            return response()->json('error');
        }

    }
}
