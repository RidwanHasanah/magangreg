<?php

namespace App\Listeners\Register;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Register\RegisterEvent;
use App\Mail\Register\RegisterEmail;
Use Mail;

class RegisterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(RegisterEvent $event)
    {
        Mail::to($event->reg->email)->send(new RegisterEmail($event->reg));
    }
}
