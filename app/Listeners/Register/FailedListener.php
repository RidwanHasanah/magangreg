<?php

namespace App\Listeners\Register;

use App\Events\Register\FailedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail; 
use App\Mail\Register\FailedEmail;

class FailedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FailedEvent  $event
     * @return void
     */
    public function handle(FailedEvent $event)
    {
        Mail::to($event->reg->email)->send(new FailedEmail($event->reg));
    }
}
