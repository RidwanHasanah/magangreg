<?php

namespace App\Listeners\Register;

use App\Events\Register\SuccessEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\Register\SuccessEmail;

class SuccessListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SuccessEvent  $event
     * @return void
     */
    public function handle(SuccessEvent $event)
    {
        Mail::to($event->interview->email)->send(new SuccessEmail($event->interview));
    }
}
