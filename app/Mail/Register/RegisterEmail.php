<?php

namespace App\Mail\Register;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Internship;

class RegisterEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $reg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Internship $reg)
    {
        $this->reg = $reg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Berhasil Mendaftar, Pendaftaran Magang Pondok IT')
                    ->markdown('emails.register.register-email');
    }
}
